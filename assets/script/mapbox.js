import {
  MAPBOX_TOKEN,
  MAPBOX_DIRECTIONS_API
} from './config/const.js';
import driverRoutes from "./data/driverRoutes.js";

// Init Variable
const latLngA = {
  lat: 3.59,
  lng: 98.67
}
const latLngB = {
  lat: 3.58,
  lng: 98.66
}
let currentDriverRouteIndex = 0

window.addEventListener('DOMContentLoaded', async () => {
  // Init Map
  mapboxgl.accessToken = MAPBOX_TOKEN;
  const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v12',
    center: [latLngA.lng, latLngA.lat],
    zoom: 15
  });

  // // Set Map max bounds
  // const bounds = [
  //   [-123.069003, 45.395273],
  //   [-122.303707, 45.612333]
  // ];
  // map.setMaxBounds(bounds);

  const getRoute = async (origin, destination) => {
    try {
      const query = await fetch(`${MAPBOX_DIRECTIONS_API}/mapbox/driving/${origin.lng},${origin.lat};${destination.lng},${destination.lat}?${
        new URLSearchParams({
          steps: false,
          geometries: 'geojson',
          annotations: 'distance,duration',
          access_token: mapboxgl.accessToken
        }).toString()
      }`)

      const json = await query.json();

      const data = json.routes[0];
      const route = data.geometry.coordinates;
      const duration = data.duration;
      const distance = data.distance;

      renderMap({
        route,
        origin,
        destination,
        distance,
        duration
      })      
    } catch (error) {
      console.log(error)
    }
  }

  const renderMap = ({
    route,
    origin,
    destination,
    distance,
    duration
  }) => {
    // Drawn Marker Destination (once)
    if (!map.getSource('markerDestination')) {
      renderMarker(destination, 'Destination')
    }

    // (Re-)Drawn Marker Origin
    renderMarker(origin, 'Origin')

    renderRoute(route)

    renderInfo(distance, 'distance')
    renderInfo(duration, 'duration')
  }

  const renderRoute = (route) => {
    const data = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: route
      }
    }

    // (Re-)Drawn Route
    if (map.getSource('route')) {
      map.getSource('route').setData(data);
    } else {
      map.addLayer({
        id: 'route',
        type: 'line',
        source: {
          type: 'geojson',
          data
        },
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': '#3887be',
          'line-width': 5,
          'line-opacity': 0.75
        }
      });
    }
  }

  const renderMarker = (coordinate, name = '') => {
    const data = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [
              coordinate.lng,
              coordinate.lat
            ]
          }
        }
      ]
    }

    if (map.getSource(`marker${name}`)) {
      map.getSource(`marker${name}`).setData(data);
    } else {
      map.addLayer({
        id: `marker${name}`,
        type: 'circle',
        source: {
          type: 'geojson',
          data
        },
        paint: {
          'circle-radius': 10,
          'circle-color': '#3887be'
        }
      });
    }
  }

  const renderInfo = (value, name= '') => {
    let finalValue = value

    if (name === 'duration')
      finalValue = Math.floor(value / 60);
    
    const el = document.getElementsByClassName(`info__value--${name}`)[0]
    el.innerText = finalValue || 0
  }

  // ## CASE 1 ##
  // await getRoute(latLngA, latLngB)
  // await renderMarker(latLngB, 'Driver')

  // driverRoutes.forEach((coordinate, i) => {
  //   setTimeout(() => {
  //     renderMarker(coordinate, 'Driver')
  //   }, 4000 * i)
  // })
  // ## END CASE 1 ##

  // ## CASE 2 ##
  // Fetch API Current Driver Location
  const getCurrentDriverLocation = () => {
    const newLatLngA = driverRoutes[currentDriverRouteIndex]
    getRoute(newLatLngA, latLngB)
  }

  // Set Interval Fetching (Temp)
  driverRoutes.forEach((coordinate, i) => {
    setTimeout(() => {
      currentDriverRouteIndex = i
      getCurrentDriverLocation()
    }, 4000 * i)
  })
  // ## END CASE 2 ##
});
