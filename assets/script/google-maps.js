import driverRoutes from "./data/driverRoutes.js";

// Init Variable
const latLngA = {
  lat: 3.59,
  lng: 98.67
}
const latLngB = {
  lat: 3.58,
  lng: 98.66
}
let currentDriverRouteIndex = 0

let map;
const markers = []

let directionsService;
let directionsRenderer;

window.addEventListener('DOMContentLoaded', () => {
  const getRoute = async (origin, destination) => {
    try {
      const request = {
        origin: origin,
        destination: destination,
        travelMode: 'DRIVING'
      };

      directionsService.route(request, (result, status) => {
        if (status == 'OK') {
          console.log('28 - result: ', result)

          const distance = result.routes[0].legs[0].distance.value;
          const duration = result.routes[0].legs[0].duration.value;
          // const distance = result.routes[0].legs[0].distance.text;
          // const duration = result.routes[0].legs[0].duration.text;

          renderMap({
            route: result,
            origin,
            destination,
            distance,
            duration
          })
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  const renderMap = ({
    route,
    origin,
    destination,
    distance,
    duration
  }) => {
    renderRoute(route)

    renderInfo(distance, 'distance')
    renderInfo(duration, 'duration')
  }

  const renderRoute = (route) => {
    directionsRenderer.setDirections(route);
  }

  const renderMarker = (coordinate, name = '') => {
    const markerIndex = markers.findIndex(item => item.slug === `marker${name}`)

    if (markerIndex !== -1) {
      markers[markerIndex].marker.setPosition(coordinate)
    } else {
      const marker = new google.maps.Marker({
        position: coordinate,
        map,
      });    

      const data = {
        id: markers.length || 0,
        slug: `marker${name}`,
        marker
      }

      markers.push(data)
    }
  }

  const renderInfo = (value, name= '') => {
    let finalValue = value

    if (name === 'duration')
      finalValue = Math.floor(value / 60);
    
    const el = document.getElementsByClassName(`info__value--${name}`)[0]
    el.innerText = finalValue || 0
  }

  // ## CASE 1 ##
  // getRoute(latLngA, latLngB)
  // renderMarker(latLngB, 'Driver')

  // driverRoutes.forEach((coordinate, i) => {
  //   setTimeout(() => {
  //     renderMarker(coordinate, 'Driver')
  //   }, 4000 * i)
  // })
  // ## END CASE 1 ##

  // ## CASE 2 ##
  // Fetch API Current Driver Location
  const getCurrentDriverLocation = () => {
    const newLatLngA = driverRoutes[currentDriverRouteIndex]
    getRoute(newLatLngA, latLngB)
  }

  // Set Interval Fetching (Temp)
  driverRoutes.forEach((coordinate, i) => {
    setTimeout(() => {
      currentDriverRouteIndex = i
      getCurrentDriverLocation()
    }, 4000 * i)
  })
  // ## END CASE 2 ##
});

function initMap() {
  const mapOpts = {
    center: {lat: latLngA.lat, lng: latLngA.lng},
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.TERRAIN,
    styles: 
    [
      {
        "featureType": "road.local",
        "stylers": [
          {
            "weight": 4.5
          }
        ]
      }
    ]
  };

  map = new google.maps.Map(document.getElementById('map'), mapOpts);

  directionsService = new google.maps.DirectionsService();
  directionsRenderer = new google.maps.DirectionsRenderer();
  directionsRenderer.setMap(map);
}

window.initMap = initMap;