const GOOGLE_MAPS_KEY = 'AIzaSyA-dd3vALtfXvAK02ULls26h9fIQxw31XM'
const MAPBOX_TOKEN = 'pk.eyJ1IjoiZXJpY21hcnRpbnFseiIsImEiOiJjbG81OWF0ODQwOHNiMmlzMDAydnlodmZiIn0.YXBHBP-ucAAgAcCd5hCrgg'

const GOOGLE_MAPS_API = ''
const MAPBOX_DIRECTIONS_API = 'https://api.mapbox.com/directions/v5';

export {
  GOOGLE_MAPS_KEY,
  MAPBOX_TOKEN,

  GOOGLE_MAPS_API,
  MAPBOX_DIRECTIONS_API
}
