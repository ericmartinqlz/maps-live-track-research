import driverRoutes from "./data/driverRoutes.js";

// Init Variable
const latLngA = {
  lat: 3.59,
  lng: 98.67
}
const latLngB = {
  lat: 3.58,
  lng: 98.66
}
let currentDriverRouteIndex = 0

const markers = []
let currentRoute;

window.addEventListener('DOMContentLoaded', () => {
  const initMap = () => {
    const map = L.map('map', {
      center: [3.59, 98.67],
      zoom: 15
    });
  
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    return map
  }

  const getRoute = async (origin, destination) => {
    try {
      if (!!currentRoute) {
        currentRoute.setWaypoints([
          L.latLng(origin.lat, origin.lng),
          currentRoute.options.waypoints[1]
        ]);
      } else {
        currentRoute = L.Routing.control({
          waypoints: [
            L.latLng(origin.lat, origin.lng),
            L.latLng(destination.lat, destination.lng)
          ],
          lineOptions: {
            // Adding animate class - but doesn't match with Case 2
            // styles: [{className: 'animate'}] 
          },
        })
          .on('routesfound', function(e) {
            const distance = e.routes[0].summary.totalDistance;
            const duration = e.routes[0].summary.totalTime;

            renderMap({
              origin,
              destination,
              distance,
              duration
            })
          })
          .addTo(map);
      }
    } catch (error) {
      console.log(error)
    }
  }

  const renderMap = ({
    // route,
    origin,
    destination,
    distance,
    duration
  }) => {
    renderInfo(distance, 'distance')
    renderInfo(duration, 'duration')
  }

  const renderMarker = (coordinate, name = '') => {
    const markerIndex = markers.findIndex(item => item.slug === `marker${name}`)

    if (markerIndex !== -1) {
      markers[markerIndex].marker.setLatLng([coordinate.lat, coordinate.lng])
    } else {
      const marker = L.marker([coordinate.lat, coordinate.lng]).addTo(map);

      const data = {
        id: markers.length || 0,
        slug: `marker${name}`,
        marker
      }

      markers.push(data)
    }
  }

  const renderInfo = (value, name= '') => {
    let finalValue = value

    if (name === 'duration')
      finalValue = Math.floor(value / 60);
    
    const el = document.getElementsByClassName(`info__value--${name}`)[0]
    el.innerText = finalValue || 0
  }

  const map = initMap()

  // ## CASE 1 ##
  // getRoute(latLngA, latLngB)
  // renderMarker(latLngB, 'Driver')

  // driverRoutes.forEach((coordinate, i) => {
  //   setTimeout(() => {
  //     renderMarker(coordinate, 'Driver')
  //   }, 4000 * i)
  // })
  // ## END CASE 1 ##

  // ## CASE 2 ##
  // Fetch API Current Driver Location
  const getCurrentDriverLocation = () => {
    const newLatLngA = driverRoutes[currentDriverRouteIndex]
    getRoute(newLatLngA, latLngB)
  }

  // Set Interval Fetching (Temp)
  driverRoutes.forEach((coordinate, i) => {
    setTimeout(() => {
      currentDriverRouteIndex = i
      getCurrentDriverLocation()
    }, 4000 * i)
  })
  // ## END CASE 2 ##
});
